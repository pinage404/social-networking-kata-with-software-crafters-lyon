type Message = {
  user: string
  message: string
  postedTime: number
}

class Timeline {
  messages: Array<Message> = []
  subscription: Record<string, Array<string>> = {}

  post(user: string, message: string, postedTime: number): void {
    this.messages.push({ user, message, postedTime })
  }

  read(readTime: number, user?: string): ReadonlyArray<string> {
    let lastSeenUser: string | undefined = undefined
    const sortMessages = (a: Message, b: Message): number => {
      if (a.user > b.user) {
        return 1
      }
      if (a.user < b.user) {
        return -1
      }

      return b.postedTime - a.postedTime
    }

    return this.messages
      .filter((message) => message.user === user || user === undefined)
      .sort(sortMessages)
      .flatMap((message) => {
        const { user } = message
        if (user === lastSeenUser)
          return [this.#getFormattedMessage(readTime, message)]
        else {
          lastSeenUser = user
          return [user, this.#getFormattedMessage(readTime, message)]
        }
      })
  }

  #getFormattedMessage(
    readTime: number,
    { message, postedTime }: Message
  ): string {
    return `${message} (${readTime - postedTime} minutes ago)`
  }

  subscribe(user: string, otherUser: string) {
    if (!(user in this.subscription)) {
      this.subscription[user] = []
    }
    this.subscription[user].push(otherUser)
  }

  readWall(readTime: number, user: string): ReadonlyArray<string> {
    return this.messages
      .filter(
        (message) =>
          this.subscription[user].includes(message.user) ||
          message.user === user
      )
      .sort((a, b) => b.postedTime - a.postedTime)
      .map(
        (message) =>
          `${message.user} - ${this.#getFormattedMessage(readTime, message)}`
      )
  }
}

describe("timeline", () => {
  it("given Alice post a message to the timeline should be readable", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    expect(timeline.read(5)).toEqual([
      "Alice",
      "i love the weather today (5 minutes ago)",
    ])
  })

  it("given Bob post a message to the timeline should be readable", () => {
    const timeline = new Timeline()
    timeline.post("Bob", "Dawn! We lost", 1)
    expect(timeline.read(3)).toEqual(["Bob", "Dawn! We lost (2 minutes ago)"])
  })

  it("given Alice post a message and Bob post a message to the timeline should Alice can read only her message", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    expect(timeline.read(3, "Alice")).toEqual([
      "Alice",
      "i love the weather today (3 minutes ago)",
    ])
  })

  it("given Alice post two messages to the timeline should Alice can read only her messages", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Alice", "Hello", 1)
    expect(timeline.read(3, "Alice")).toEqual([
      "Alice",
      "Hello (2 minutes ago)",
      "i love the weather today (3 minutes ago)",
    ])
  })

  it("given Alice post a message and Bob post messages to the timeline should see all messages agregated by users", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    timeline.post("Bob", "Good game though", 2)
    expect(timeline.read(3)).toEqual([
      "Alice",
      "i love the weather today (3 minutes ago)",
      "Bob",
      "Good game though (1 minutes ago)",
      "Dawn! We lost (2 minutes ago)",
    ])
  })

  it("given Charlie subscribes to Alice then Charlie can view Alice's messages on his wall", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    timeline.post("Bob", "Good game though", 2)
    timeline.subscribe("Charlie", "Alice")
    expect(timeline.readWall(3, "Charlie")).toEqual([
      "Alice - i love the weather today (3 minutes ago)",
    ])
  })

  it("given Charlie subscribes to Bob then Charlie can view Bob's messages on his wall", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    timeline.post("Bob", "Good game though", 2)
    timeline.subscribe("Charlie", "Bob")
    expect(timeline.readWall(3, "Charlie")).toEqual([
      "Bob - Good game though (1 minutes ago)",
      "Bob - Dawn! We lost (2 minutes ago)",
    ])
  })

  it("given Charlie subscribes to Bob and post a message then Charlie can view Bob's messages on his wall", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    timeline.post("Bob", "Good game though", 2)
    timeline.post("Charlie", "I'm in New York today", 3)
    timeline.subscribe("Charlie", "Bob")
    expect(timeline.readWall(3, "Charlie")).toEqual([
      "Charlie - I'm in New York today (0 minutes ago)",
      "Bob - Good game though (1 minutes ago)",
      "Bob - Dawn! We lost (2 minutes ago)",
    ])
  })

  it("given Charlie subscribes to Bob and post a message and Bob subscribes to Alice then Charlie can view Bob's messages on his wall", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    timeline.post("Bob", "Good game though", 2)
    timeline.post("Charlie", "I'm in New York today", 3)
    timeline.subscribe("Charlie", "Bob")
    timeline.subscribe("Bob", "Alice")
    expect(timeline.readWall(3, "Charlie")).toEqual([
      "Charlie - I'm in New York today (0 minutes ago)",
      "Bob - Good game though (1 minutes ago)",
      "Bob - Dawn! We lost (2 minutes ago)",
    ])
  })

  it("given Charlie subscribes to Bob and post a message and Bob subscribes to Alice then Charlie can view Bob's messages on his wall", () => {
    const timeline = new Timeline()
    timeline.post("Alice", "i love the weather today", 0)
    timeline.post("Bob", "Dawn! We lost", 1)
    timeline.post("Bob", "Good game though", 2)
    timeline.post("Charlie", "I'm in New York today", 3)
    timeline.subscribe("Charlie", "Bob")
    timeline.subscribe("Charlie", "Alice")
    timeline.subscribe("Bob", "Alice")
    expect(timeline.readWall(3, "Charlie")).toEqual([
      "Charlie - I'm in New York today (0 minutes ago)",
      "Bob - Good game though (1 minutes ago)",
      "Bob - Dawn! We lost (2 minutes ago)",
      "Alice - i love the weather today (3 minutes ago)",
    ])
  })
})
